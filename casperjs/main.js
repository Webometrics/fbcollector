var casper = require('casper').create({
    pageSettings: {
        userAgent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
		loadImages: false,
    },
	clientScripts:  [
		'includes/jquery.min.js'
	],
	//logLevel: 'info',
	verbose: true
});

var fs = require('fs');

function allPosts(hashTag) { 
	var commentPosts = [];
	var sharedPosts = [];
	var emotionPosts = [];
	var totalEmotionPosts = [];
	
	var mainJson = [];
	
	$(".userContentWrapper").each(function(index, mainContent) {
		var elementJson = {};
		
		$(mainContent).find(".fwb").each(function(indexTitle, elementTitle) {
			elementJson.Username = $(elementTitle).text();
		});
		
		if (Object.values($(mainContent).find("._3twv"))[0] != 0) {
			elementJson.Confirmed = true;
		} else {
			elementJson.Confirmed = false;
		}
		
		var timePost = $(mainContent).find("._5ptz").attr("title");
			
			timePost = timePost.split(", ");
			var date = timePost[0].split(".");
			var day = Number(date[0]);
			var month = Number(date[1]);
			var year = Number(date[2]);

			var watch = timePost[1].split(":");
			var hour = Number(watch[0]);
			var minutes = Number(watch[1]);

			var unixTime = new Date (year, month, day, hour, minutes);

		elementJson["Date"] = unixTime.getTime();
		
		
		elementJson["Request"] = hashTag;
		
		var messagePost = "";
		$(mainContent).find(".userContent").find("p").each(function(index, elementMessage) {
			messagePost = messagePost + $(elementMessage).clone().children().remove().end().text();
		});
		elementJson.Message = messagePost;
		
		var showText = "";
		$(mainContent).find(".userContent").find(".text_exposed_show").find("p").each(function(index, elementMessage) {
			showText = showText + $(elementMessage).clone().children().remove().end().text();
		});
		elementJson.Message = elementJson.Message + showText;
		
		var hashTagsJson = [];
		$(mainContent).find(".userContent").find("._58cn").each(function(index, elementTagPost) {
			hashTagsJson.push($(elementTagPost).text());
		});
		elementJson.Tags = hashTagsJson;
		
		
		var emotionsJson = {};
		$(mainContent).find("._66lg").find("._1n9l").each(function(index, elementEmotion) {
			var typeOfemotion = $(elementEmotion).attr("aria-label").split(": ")[0];
			var countOfemotion = $(elementEmotion).attr("aria-label").split(": ")[1];
			
			if (!!countOfemotion != 0 && countOfemotion.search(',') != '-1') {
				countOfemotion = countOfemotion.replace(",", ".");
				countOfemotion = (parseFloat(countOfemotion) * 1000);
			}
			
			if (typeOfemotion == "Нравится") {
				emotionsJson.Like = countOfemotion;
			} else if (typeOfemotion == "Супер") {
				emotionsJson.Love = countOfemotion;
			} else if (typeOfemotion == "Ха-ха") {
				emotionsJson.Haha = countOfemotion;
			} else if (typeOfemotion == "Ух ты!") {	
				emotionsJson.Wow = countOfemotion;
			} else if (typeOfemotion == "Сочувствую") {
				emotionsJson.Sad = countOfemotion;
			} else if (typeOfemotion == "Возмутительно"){
				emotionsJson.Angry = countOfemotion;
			}
		});
		elementJson.Emotions = emotionsJson;
		
		elementJson.TotalEmotions = "";
		var totalEmotionPost = $(mainContent).find("._3dlg").text();
		
		if (!!totalEmotionPost != 0 && totalEmotionPost.search(',') != '-1') {
			totalEmotionPost = totalEmotionPost.replace(",", ".");
			totalEmotionPost = (parseFloat(totalEmotionPost) * 1000);
		}
			
		if (!!totalEmotionPost == false) elementJson.TotalEmotions = "";
		else elementJson.TotalEmotions = totalEmotionPost;
		
		
		elementJson.Comment = "";
		var commentPost = $(mainContent).find("._3hg-").text();
		commentPost = commentPost.split(": ")[1];
		
		if (!!commentPost != 0 && commentPost.search(',') != '-1') {
			commentPost = commentPost.replace(",", ".");
			commentPost = (parseFloat(commentPost) * 1000);
		}
		
		if (!!commentPost == false) elementJson.Comment = "";
		else elementJson.Comment = commentPost;
		
		elementJson.Share = "";
		var sharedPost = $(mainContent).find("._3rwx").text();
		sharedPost = sharedPost.split(": ")[1];
		
		if (!!sharedPost != 0 && sharedPost.search(',') != '-1') {
			sharedPost = sharedPost.replace(",", ".");
			sharedPost = (parseFloat(sharedPost) * 1000);
		}
		
		if (!!sharedPost == false) elementJson.Share = "";
		else elementJson.Share = sharedPost;
		
		mainJson.push(elementJson);

	});
	return JSON.stringify(mainJson, null, '\t');
}

function deleteBrowseResultsContainerAndBelowFold () {
	$("#BrowseResultsContainer").remove();
	$("#u_ps_0_4_0_browse_result_below_fold").remove();
}

function deletefbBrowseScrollingPagerContainer(id, to) {
	for (var i = 0; i <= to; i++) {
		var curId = id + i;
		var idSelector = "#fbBrowseScrollingPagerContainer" + curId;
		$(idSelector).remove();
	}	
}

var debagerCounter  = 0;

var getContent = function() {
	
	casper.then(function() {
		this.echo("#########################################################################");
		idUpper = idCounter;
		while (true) {
			if (this.exists("#fbBrowseScrollingPagerContainer" + idUpper)) {
				idUpper++;
			} else {
				idUpper--;
				break;
			}
		}
	});
	
	casper.then(function() {
		
		this.echo("The idUpper : " + idUpper);
		this.echo("The idCounter : " + idCounter);
		var contentHtml = this.getHTML();
		fs.write('debugOnPasing/WhatsPostWeWait.html', "\uFEFF" + contentHtml, 'w');
		
		this.waitUntilVisible("#fbBrowseScrollingPagerContainer" + idUpper, function() {
			var contentHtml = this.getHTML();
			fs.write('debugOnPasing/waitUntilVisible.html', "\uFEFF" + contentHtml, 'w');
			this.echo("THE STEP: -START- waitUntilVisible is started -END-");
			
			var response = this.evaluate(allPosts, hashTag);
			response = response.slice(0, -1);
			response = response.slice(1);
			response = response.concat(",");
			fs.write('demoResults.json', "\uFEFF" + response, 'a');
			
			this.echo("THE STEP: -END- WaitUntilVisible is done -END-");
		}, function() {
			if (this.exists(".phm") == true) {
				this.echo(" TRUE TRUE WHERE WE PARSEEEEE");
			} else {
				this.echo("Problems with Internet"); 
				this.capture('Promlem capture.png');
			}
		}, 50 * 1000);
		
	});
	
	casper.then(function() {
		idWaiter = idUpper;
		while (true) {
			if (this.exists("#fbBrowseScrollingPagerContainer" + idWaiter)) {
				idWaiter++;
			} else {
				idWaiter--;
				break;
			}
		}
		var t = this.exists("#fbBrowseScrollingPagerContainer" + idWaiter);
		this.echo("THE STEP AFTER PARSE idWaiter TRUE TURE********************** " + t + " **************************");
		this.echo("THE STEP AFTER PARSE idWaiter ********************** " + idWaiter + " **************************");
		this.echo("THE STEP AFTER PARSE idUpper ********************** " + idUpper + " **************************");
	});
	
	/////////////////// -------------------- The biggest stop --------------------------
	
	casper.then(function() {
		if (this.exists(".phm") == false) {
			this.waitUntilVisible("#fbBrowseScrollingPagerContainer" + idWaiter, function() {
				
				this.echo("THE POST AFTER PARSE VISIBLE " + idWaiter + " !!-----!!");
				
			}, function() {
				
				this.echo("THE STEP AFTER PARSE ISN'T VISIBLE " + idWaiter + " !!-----!!");
				
			}, 300 * 1000);
		}
	});
	
	
	casper.then(function() {
		this.echo("Srolled to BOTTOM"); //Srolled to BOTTOM
		this.scrollToBottom();
	});
	
	casper.then(function() {
		this.wait(10 * 1000);
		var contentHtml = this.getHTML();
		fs.write('debugOnPasing/HtmlAfterScroll.html', "\uFEFF" + contentHtml, 'w');
	});
	
	casper.then(function() {
		idWaiter = idUpper;
		while (true) {
			if (this.exists("#fbBrowseScrollingPagerContainer" + idWaiter)) {
				idWaiter++;
			} else {
				idWaiter--;
				break;
			}
		}
		var t = this.exists("#fbBrowseScrollingPagerContainer" + idWaiter);
		this.echo("THE STEP idWaiter TRUE TURE********************** " + t + " **************************");
		this.echo("THE STEP idWaiter ********************** " + idWaiter + " **************************");
		this.echo("THE STEP idUpper ********************** " + idUpper + " **************************");
	});
	
	casper.then(function() {
		var contentHtml = this.getHTML();
		fs.write('debugOnPasing/HotPlace.html', "\uFEFF" + contentHtml, 'w');
		
		/*this.evaluate(deletefbBrowseScrollingPagerContainer, idCounter, idUpper - idCounter);
		idCounter = idUpper;
		idCounter++;*/
		
		if (this.exists(".phm") == false) {
			this.waitUntilVisible("#fbBrowseScrollingPagerContainer" + idWaiter, function() {
				
				this.echo("THE STEP " + idCounter + " : --------------Remove INITIAL POSTS-----------------");
				this.evaluate(deletefbBrowseScrollingPagerContainer, idCounter, idUpper - idCounter);
				idCounter = idUpper;
				idCounter++;
				
			}, function() {
				/*this.then(function(){
					this.scrollToBottom();
				});
				
				this.then(function(){
					this.scrollToBottom();
				});
				this.scrollToBottom();
				this.wait(100*1000, function() {
					
				});
				this.scrollToBottom();*/
				
				this.echo("THE STEP " + idCounter + " : -------------- DOESN'T Remove INITIAL POSTS-----------------");
				this.evaluate(deletefbBrowseScrollingPagerContainer, idCounter, idUpper - idCounter);
				idCounter = idUpper;
				idCounter++;
				
			}, 300 * 1000);
		}
	});
	
	casper.then(function() {
		while (true) {
			if (this.exists("#fbBrowseScrollingPagerContainer" + idWaiter)) {
				idWaiter++;
			} else {
				idWaiter--;
				break;
			}
		}
		var t = this.exists("#fbBrowseScrollingPagerContainer" + idWaiter);
		this.echo("THE LAST!!!!! STEP idWaiter TRUE TURE********************** " + t + " **************************");
		this.echo("THE LAST!!!!! idWaiter ********************** " + idWaiter + " **************************");
		this.echo("THE LAST!!!!! idUpper ********************** " + idUpper + " **************************");
	});
	
	casper.then(function() {
		var contentHtml = this.getHTML();
		fs.write('debugOnPasing/HtmlAfterDeletedidWaiter.html', "\uFEFF" + contentHtml, 'w');
	});
	
	casper.then(function() {
		if (this.exists(".phm") == false) {
			this.waitUntilVisible("#fbBrowseScrollingPagerContainer" + idWaiter, function() {
				
				this.echo("THE POST VISIBLE " + idWaiter + " !!-----!!");
				
			}, function() {
				
				this.echo("THE STEP ISN'T VISIBLE " + idWaiter + " !!-----!!");
				
			}, 300 * 1000);
		} 
	});
	
	casper.then(function() {
		var contentHtml = this.getHTML();
		fs.write('debugOnPasing/whetherDeleted.html', "\uFEFF" + contentHtml, 'w');  //whether deleted?
		this.echo("############################################################################");
	});
	
	casper.then(function() {
		if (this.exists(".phm") == false) {
			getContent();
		} else {
			var contentHtml = this.getHTML();
			fs.write('debugOnPasing/LastHTML'+ debagerCounter +'.html', "\uFEFF" + contentHtml, 'w');
			debagerCounter++;
		}
	});
	
}

var stream = fs.open('Links.txt', 'r');

var Links = [];
while (!stream.atEnd()) {
    var line = stream.readLine();
    Links.push(line);
}
stream.close();

var idCounter = 0;
var idUpper = 0;
var idWaiter = 0;

casper.start().thenOpen("https://facebook.com", function() {
	this.evaluate(function(){
        document.getElementById("email").value="89650383509";
		document.getElementById("pass").value="daurenqwerty";
		document.getElementById("loginbutton").children[0].click();
    });
});

casper.then(function() {
	fs.write('results.json', "\uFEFF" + "[", 'a');
});

var hashTag = "#putin";
var request = "";

if (hashTag[0] == "#") {
	request = hashTag.slice(1);
}

var headLink = "https://www.facebook.com/search/str/%23";
var ref = "https://www.facebook.com/search/str/%23" + request + "/stories-keyword/stories-public";
var uniqRef = "https://www.facebook.com/search/str/%23Putin/stories-keyword/";
var fullLinks = [];

var betaLink = [];
betaLink.push(uniqRef);

//fullLinks.push(uniqRef);
for (var i = 0; i < Links.length; i++) {
	var linkk = headLink + request + Links[i];
	fullLinks.push(linkk);
}

var divider = [];
for (var i = 1; i < fullLinks.length/5; i++) {
	divider.push(5 * i);
} 

if (fullLinks.length%5 != 0) {
	divider.push(fullLinks.length);
}

/// -------------------- Part 1 ----------------------
var partOneLinks = [];
for (var i = 0; i < divider[0]; i++) {
	partOneLinks.push(fullLinks[i]);
}

/// -------------------- Part 2 ----------------------
var partTwoLinks = [];
for (var i = divider[0]; i < divider[1]; i++) {
	partTwoLinks.push(fullLinks[i]);
}

/// -------------------- Part 3 ----------------------
var partThreeLinks = [];
for (var i = divider[1]; i < divider[2]; i++) {
	partThreeLinks.push(fullLinks[i]);
}

/// -------------------- Part 4 ----------------------
var partFourLinks = [];
for (var i = divider[2]; i < divider[3]; i++) {
	partFourLinks.push(fullLinks[i]);
}

/// -------------------- Part 5 ----------------------
var partFiveLinks = [];
for (var i = divider[3]; i < divider[4]; i++) {
	partFiveLinks.push(fullLinks[i]);
}

/// -------------------- Part 6 ----------------------
var partSixLinks = [];
for (var i = divider[4]; i < divider[5]; i++) {
	partSixLinks.push(fullLinks[i]);
}

/// ------------------ Part 7 ----------------------
var partSevenLinks = [];
for (var i = divider[5]; i <= divider[6]; i++) {
	partSevenLinks.push(fullLinks[i]);
}


casper.each(betaLink, function(sself, linkk) {
	
	sself.then(function() {
		
		this.open(linkk).then(function() {
			idCounter = 0;
			idUpper = 0;
			idWaiter = 0;
			var response = this.evaluate(allPosts, hashTag);
			response = response.slice(0, -1);
			response = response.slice(1);
			response = response.concat(",");
			this.echo("--------------------------I'm Here-----------------------------"); 
			fs.write('demoResults.json', "\uFEFF" + response, 'a');
			
		});
		
	});
	
	sself.then(function() {
		this.scrollToBottom();
	});
	
	sself.then(function() {
		this.wait(5 * 1000);
	});
	
	sself.then(function() {
		this.evaluate(deleteBrowseResultsContainerAndBelowFold);
	});
	
	sself.then(function() {
		getContent();
	});
	
	sself.then(function() {
		this.echo("--------I'm trying to clear-------");
		this.clear();
		this.wait(20*1000, function() {
			this.echo("--------I'm cleared-------");
		});
	});
	
	sself.then(function() {
		this.echo("--------I'm trying to clearCache-------");
		this.clearCache();
		this.wait(20*1000, function() {
			this.echo("--------I'm closedCache-------");
		});
	});
	
	sself.then(function() {
		this.echo("--------I'm trying to close-------");
		this.page.close();
		this.wait(20*1000, function() {
			this.echo("--------I'm closed-------");
		});
	});
	
});

/*casper.then(function() {
	fs.write('results.json', "\uFEFF" + "]", 'a');
});*/

casper.then(function() {
	this.echo("-----------**********----------- I'm Finished ---------**********--------");
});

casper.run();