const puppeteer = require('puppeteer');
const fs = require('fs');
const scrollPageToBottom = require("puppeteer-autoscroll-down");
const readline = require('readline');  


function fetchData(hashTag) {
	var commentPosts = [];
	var sharedPosts = [];
	var emotionPosts = [];
	var totalEmotionPosts = [];
	
	var mainJson = [];
	
	$(".userContentWrapper").each(function(index, mainContent) {
		var elementJson = {};
		
		$(mainContent).find(".fwb").each(function(indexTitle, elementTitle) {
			elementJson.Username = $(elementTitle).text();
		});
		
		if (Object.values($(mainContent).find("._3twv"))[0] != 0) {
			elementJson.Confirmed = true;
		} else {
			elementJson.Confirmed = false;
		}
		
		elementJson['Location'] = '';
		var loc_v2 = '';
		var w = 1;
		var check = $(mainContent).find('.fwn.fcg').find('.fcg').text();
		if (check.search('город') != -1) {
			$(mainContent).find('.fwn.fcg').find('.fcg').find('.profileLink').each(function(index, mainnContent) {
				if (w == 2) {
					var l = $(mainnContent).text().split(", ");
					loc_v2 = l[0];
				}
				w++;
			});
		}
		
		var e = 1;
		var loc_v1 = '';
		$(mainContent).find('.fcg').find('._5pcq').each(function(index, mainContent) {
			if (e == 2) {
				loc_v1 = $(mainContent).text();
			}
			e++;
		});
		
		if (loc_v1 != '') {
			elementJson['Location'] = loc_v1;
		} else if (loc_v2 != '') {
			elementJson['Location'] = loc_v2;
		}
		
		
		var timePost = $(mainContent).find("._5ptz").attr("title");
		if (typeof timePost !== 'string') {
			str = '1556456880000';
			elementJson["Date"] = str;
		} else {
			timePost = timePost.split(", ");
			var date = timePost[0].split(".");
			var day = Number(date[0]);
			var month = Number(date[1]);
			var year = Number(date[2]);

			var watch = timePost[1].split(":");
			var hour = Number(watch[0]);
			var minutes = Number(watch[1]);

			var unixTime = new Date (year, month, day, hour, minutes);
			elementJson["Date"] = unixTime.getTime();
		}
		
		
		elementJson["Request"] = hashTag;
		
		var messagePost = "";
		$(mainContent).find(".userContent").find("p").each(function(index, elementMessage) {
			messagePost = messagePost + $(elementMessage).clone().children().remove().end().text();
		});
		elementJson.Message = messagePost;
		
		var showText = "";
		$(mainContent).find(".userContent").find(".text_exposed_show").find("p").each(function(index, elementMessage) {
			showText = showText + $(elementMessage).clone().children().remove().end().text();
		});
		elementJson.Message = elementJson.Message + showText;
		
		var hashTagsJson = [];
		$(mainContent).find(".userContent").find("._58cn").each(function(index, elementTagPost) {
			hashTagsJson.push($(elementTagPost).text());
		});
		elementJson.Tags = hashTagsJson;
		
		
		var emotionsJson = {};
		$(mainContent).find("._66lg").find("._1n9l").each(function(index, elementEmotion) {
			var typeOfemotion = $(elementEmotion).attr("aria-label").split(": ")[0];
			var countOfemotion = $(elementEmotion).attr("aria-label").split(": ")[1];
			
			if (!!countOfemotion != 0 && (countOfemotion.search(',') != '-1' || countOfemotion.search('тыс') != '-1' || countOfemotion.search('млн') != '-1')) {
				countOfemotion = countOfemotion.replace(",", ".");
				if (countOfemotion.search('млн') != '-1') {
					countOfemotion = (parseFloat(countOfemotion) * 1000000);
				} else {
					countOfemotion = (parseFloat(countOfemotion) * 1000);
				}
			}
			
			if (typeOfemotion == "Нравится") {
				emotionsJson.Like = countOfemotion;
			} else if (typeOfemotion == "Супер") {
				emotionsJson.Love = countOfemotion;
			} else if (typeOfemotion == "Ха-ха") {
				emotionsJson.Haha = countOfemotion;
			} else if (typeOfemotion == "Ух ты!") {	
				emotionsJson.Wow = countOfemotion;
			} else if (typeOfemotion == "Сочувствую") {
				emotionsJson.Sad = countOfemotion;
			} else if (typeOfemotion == "Возмутительно"){
				emotionsJson.Angry = countOfemotion;
			}
		});
		elementJson.Emotions = emotionsJson;
		
		elementJson.TotalEmotions = "";
		var totalEmotionPost = $(mainContent).find("._3dlg").text();
		
		if (!!totalEmotionPost != 0 && (totalEmotionPost.search(',') != '-1' || totalEmotionPost.search('тыс') != '-1' || totalEmotionPost.search('млн') != '-1')) {
			totalEmotionPost = totalEmotionPost.replace(",", ".");
			if (totalEmotionPost.search('млн') != '-1') {
				totalEmotionPost = (parseFloat(totalEmotionPost) * 1000000);
			} else {
				totalEmotionPost = (parseFloat(totalEmotionPost) * 1000);
			}
		}
			
		if (!!totalEmotionPost == false) elementJson.TotalEmotions = "";
		else elementJson.TotalEmotions = totalEmotionPost;
		
		
		elementJson.Comment = "";
		var commentPost = $(mainContent).find("._3hg-").text();
		commentPost = commentPost.split(": ")[1];
		
		if (!!commentPost != 0 && (commentPost.search(',') != '-1' || commentPost.search('тыс') != '-1' || commentPost.search('млн') != '-1')) {
			commentPost = commentPost.replace(",", ".");
			if (commentPost.search('млн') != '-1') {
				commentPost = (parseFloat(commentPost) * 1000000);
			} else {
				commentPost = (parseFloat(commentPost) * 1000);
			}
		}
		
		if (!!commentPost == false) elementJson.Comment = "";
		else elementJson.Comment = commentPost;
		
		elementJson.Share = "";
		var sharedPost = $(mainContent).find("._3rwx").text();
		sharedPost = sharedPost.split(": ")[1];
		
		if (!!sharedPost != 0 && (sharedPost.search(',') != '-1' || sharedPost.search('тыс') != '-1' || sharedPost.search('млн') != '-1')) {
			sharedPost = sharedPost.replace(",", ".");
			if (sharedPost.search('млн') != '-1') {
				sharedPost = (parseFloat(sharedPost) * 1000000);
			} else {
				sharedPost = (parseFloat(sharedPost) * 1000);
			}
		}
		
		if (!!sharedPost == false) elementJson.Share = "";
		else elementJson.Share = sharedPost;
		
		mainJson.push(elementJson);

	});
	response = JSON.stringify(mainJson, null, '\t');
	response = response.slice(0, -1);
	response = response.slice(1);
	response = response.concat(",");
	
	return Promise.resolve(response);
}

function deleteBrowseResultsContainerAndBelowFold () {
	$("#BrowseResultsContainer").remove();
	$("#u_ps_0_3_0_browse_result_below_fold").remove();
}

function deletefbBrowseScrollingPagerContainer(id, to) {
	for (var i = 0; i <= to; i++) {
		var curId = id + i;
		var idSelector = "#fbBrowseScrollingPagerContainer" + curId;
		$(idSelector).remove();
	}	
}

function isVisible(selector) {
  return ((document.querySelector(selector) && document.querySelector(selector).clientHeight != 0) || ($(".phm._64f").length == 1));
}

var idCounter = 0;
var idUpper = 0;
var idWaiter = 0;

var getLastNumberOfExistingPost = async (id, pagee) => {
	
	while (true) {
		if (!!(await pagee.$("#fbBrowseScrollingPagerContainer" + id))) {
			id++;
		} else {
			id--;
			break;
		}
	}
	
	return id;
};


var isExist = async (page) => {
	
	if (!!(await pagee.$("#fbBrowseScrollingPagerContainer" + id))) {
		return true;
	} else {
		return false;
	}
	
};

var theLastAct = async (page) => {
	
	if (!!(await page.$(".phm._64f")) == false) {
		await scrapeTheRestContent(page);
	} else {
		var content = await page.content();
		await fs.writeFile("theEnd.html", "\uFEFF" + content, function(err) {
			if(err) {
				return console.log(err);
			}
			console.log("The last HTML file was saved!");
		});
		idCounter = 0;
		idUpper = 0;
		idWaiter = 0;
	}
	
};

var writeFileCostom = async (nameFile, content, message) => {
	
	await fs.writeFile(nameFile, content, 'utf8', (err) => {
		if(err) return console.log(err);
			console.log(message);
	});
	
};

var appendFileCostom = async (nameFile, content, message) => {
	
	await fs.appendFile(nameFile, content, 'utf8', (err) => {
		if(err) return console.log(err);
			console.log(message);
	});
	
};

var getDebugHTMLasFile = async (page, filePath, message) => {
	var content = await page.content();
	await writeFileCostom(filePath, content, message);
};

var gettDebugHTMLasFile = async (page, filePath, message) => {
	var content = await page.content();
	await writeFileCostom(filePath, content, message);
};

var waitUntilVisble = async () => {
	
};

function getAllExistContents() {
	return $(".userContentWrapper").length;
}

var scrapeTheRestContent = async (page) => {
	console.log("#########################################################################");
	
	await getDebugHTMLasFile(page, "debugOnPasing/######.html", "Before" + idUpper);
	
	idUpper = await getLastNumberOfExistingPost(idCounter, page);
	console.log(idUpper);
	content = await page.content();
	await writeFileCostom("debugOnPasing/hot.html", content, "Hotttttt");
	await page.waitForFunction(isVisible, {timeout: 50*1000}, ('#fbBrowseScrollingPagerContainer' + idUpper))
		.then(async () => {
			content = await page.content();
			await writeFileCostom("debugOnPasing/HotWait.html", content, "hot success");
		})
		.catch(async () => {
			content = await page.content();
			gettDebugHTMLasFile(page, "debugOnPasing/problemHotWait.html", "hot problem")
		});
		
	var results = await page.evaluate(fetchData, "putin");
	await appendFileCostom("data.json", results, "The file " + idUpper + " was saved!");
	var countOfMetrics = await page.evaluate(getAllExistContents);
	
	metric = await page.metrics();
	post = metric["Timestamp"];
	var s = (post - pre)/60; 
	pre = post;
	sum = sum + s;
	countOfMetricsSum = countOfMetricsSum + countOfMetrics;
	
	await appendFileCostom("Metrics/successMetricsMain.text",  countOfMetricsSum + ' ' + sum + '\n', "Success Mtrics file was saved");
	
	// We'll wait posts after get new information ------------------------1111111---------------------------
	if (!!(await page.$(".phm._64f")) == false) {
		idWaiter = await getLastNumberOfExistingPost(idUpper, page);
		await page.waitForFunction(isVisible, {timeout: 100*1000}, ('#fbBrowseScrollingPagerContainer' + idWaiter))
			.then(async () => {
				content = await page.content();
				await writeFileCostom("debugOnPasing/successFirstWait.html", content, "The first waiter has success");
			})
			.catch(async () => {
				content = await page.content();
				gettDebugHTMLasFile(page, "debugOnPasing/problemFirstWait.html", "The first waiter has problem")
			});
	}
	
	//await page.waitFor(5 * 1000);
	
	await scrollPageToBottom(page);
	//await page.waitFor(3 * 1000);
	
	/*log*/
	await getDebugHTMLasFile(page, "debugOnPasing/afterSrollOne.html", "The afterSrollOne HTML file was saved!");
	/*log*/
	
	// We'll wait after first scrollPageToBottom ------------------------2222222---------------------------
	if (!!(await page.$(".phm._64f")) == false) {
		idWaiter = await getLastNumberOfExistingPost(idUpper, page);
		await page.waitForFunction(isVisible, {timeout: 100*1000}, ('#fbBrowseScrollingPagerContainer' + idWaiter))
			.then(async () => {
				content = await page.content();
				await writeFileCostom("debugOnPasing/successSecondWait.html", content, "The Second waiter has success");
			})
			.catch(async () => {
				content = await page.content();
				await writeFileCostom("debugOnPasing/problemSecondWait.html", content, "The Second waiter has problem");
			});
	}
	
	if (!!(await page.$(".phm._64f")) == false) {
		//await page.waitFor(5 * 1000);
		await page.evaluate(deletefbBrowseScrollingPagerContainer, idCounter, idUpper - idCounter);
		idCounter = idUpper;
		idCounter++;
	}
	
	await scrollPageToBottom(page);
	//await page.waitFor(3 * 1000);
	
	/*log*/
	await getDebugHTMLasFile(page, "debugOnPasing/afterSrollTwo.html", "The afterSrollTwo HTML file was saved!");
	/*log*/
	
	// We'll wait after second scrollPageToBottom ------------------------333333---------------------------
	if (!!(await page.$(".phm")) == false) {
		idWaiter = await getLastNumberOfExistingPost(idCounter, page);
		await page.waitForFunction(isVisible, {timeout: 100*1000}, ('#fbBrowseScrollingPagerContainer' + idWaiter))
			.then(async () => {
				content = await page.content();
				await writeFileCostom("debugOnPasing/successThirdWait.html", content, "The Third waiter has success");
			})
			.catch(async () => {
				content = await page.content();
				await writeFileCostom("debugOnPasing/problemThirdWait.html", content, "The Third waiter has problem");
			});
	}
	//await page.waitFor(5 * 1000);
	await theLastAct(page);
};

var Links = [];
var ref = "https://www.facebook.com/search/str/%23"; 
var request = "putin";
var uniqRef = "https://www.facebook.com/search/str/%23putin/stories-keyword/stories-public";
var uniqRef1 = "https://www.facebook.com/search/str/%23putin/stories-keyword/stories-public?epa=FILTERS&filters=eyJycF9jcmVhdGlvbl90aW1lIjoie1wibmFtZVwiOlwiY3JlYXRpb25fdGltZVwiLFwiYXJnc1wiOlwie1xcXCJzdGFydF95ZWFyXFxcIjpcXFwiMjAxOFxcXCIsXFxcInN0YXJ0X21vbnRoXFxcIjpcXFwiMjAxOC0xXFxcIixcXFwiZW5kX3llYXJcXFwiOlxcXCIyMDE4XFxcIixcXFwiZW5kX21vbnRoXFxcIjpcXFwiMjAxOC0xMlxcXCIsXFxcInN0YXJ0X2RheVxcXCI6XFxcIjIwMTgtMS0xXFxcIixcXFwiZW5kX2RheVxcXCI6XFxcIjIwMTgtMTItMzFcXFwifVwifSJ9";

var loc = "https://www.facebook.com/search/str/%23putin/stories-keyword/stories-public?epa=FILTERS&filters=eyJycF9sb2NhdGlvbiI6IntcIm5hbWVcIjpcImxvY2F0aW9uXCIsXCJhcmdzXCI6XCIxMDgxMzEwODU4ODYxMTZcIn0ifQ%3D%3D";

var getFullLinks = async () => {
	
	const readInterface = readline.createInterface({  
		input: fs.createReadStream('Links.txt')
	});
	
	
	readInterface.on('line', function(line) {  
		Links.push(ref + request + line);
		//console.log(Links.length);
	});
	
};


var metric;
var pre;
var post;
var sum = 0;
var countOfMetricsSum = 0;

(async () => {
	
	await getFullLinks();
	//Links.push(loc);
	
	const browser = await puppeteer.launch({args: ['--disable-dev-shm-usage']});
	const page = await browser.newPage();
	
	metric = await page.metrics();
	pre = metric["Timestamp"];
	post = pre;
	
	await page.goto('https://facebook.com');
	await page.setViewport({width: 1920, height: 1080});
	
	await page.type('#email', '89650383509'); 
	await page.type('#pass', 'daurenqwerty', {delay: 100}); 
	await page.click('[aria-label="Войти"]');
	
	/*await page.waitFor(3 * 1000);
	await page.goto(uniqRef1);
	await getDebugHTMLasFile(page, "test/" + 100 + ".html", "The" + 100);
	**/
	
	await appendFileCostom("data.json", '[', "Add [");
	
	for (var i = 0; i < Links.length; i++) {
		await page.waitFor(5 * 1000);
		await page.goto(Links[i], {waitUntil: 'networkidle2'});
		await page.addScriptTag({path: require.resolve('jquery')});
		
		await getDebugHTMLasFile(page, "test/" + i + ".html", "The" + i);
	
		const result = await page.evaluate(fetchData, "putin");
		var countOfMetrics = await page.evaluate(getAllExistContents);
		
		await appendFileCostom("data.json", result, "The first data was saved!");
		
		metric = await page.metrics();
		post = metric["Timestamp"];
		var s = (post - pre)/60; 
		pre = post;
		
		countOfMetricsSum = countOfMetricsSum + countOfMetrics;	
		sum = sum + s;
			
		await appendFileCostom("Metrics/successMetricsMain.text",  countOfMetricsSum + ' ' + sum + '\n', "Success Mtrics file was saved");
	
		const lastPosition = await scrollPageToBottom(page);
	
		await page.waitForFunction(isVisible, {timeout: 50*1000}, '#fbBrowseScrollingPagerContainer0');
	
		await page.evaluate(deleteBrowseResultsContainerAndBelowFold);
	
		await scrapeTheRestContent(page);
	}
	
	await browser.close();
})();


