const puppeteer = require('puppeteer');
const fs = require('fs');
const scrollPageToBottom = require("puppeteer-autoscroll-down");
const readline = require('readline');  

function fetchData() {
	var commentPosts = [];
	var sharedPosts = [];
	var emotionPosts = [];
	var totalEmotionPosts = [];
	
	var mainJson = [];
	
	var mainContent = $(".userContentWrapper").first();
	var elementJson = {};
	
	$(mainContent).find(".fwb").each(function(indexTitle, elementTitle) {
		elementJson.Username = $(elementTitle).text();
	});
	
	if ($(mainContent).find("._19s-").find("._3d2h").length) {
		elementJson.Confirmed = false;
	} else {
		elementJson.Confirmed = true;
	}
	
	var timePost = $(mainContent).find("._5ptz").attr("title");
	if (typeof timePost !== 'string') {
		str = '1556456880000';
		elementJson["Date"] = str;
	} else {
		timePost = timePost.split(", ");
		var date = timePost[0].split(".");
		var day = Number(date[0]);
		var month = Number(date[1]);
		var year = Number(date[2]);

		var watch = timePost[1].split(":");
		var hour = Number(watch[0]);
		var minutes = Number(watch[1]);

		var unixTime = new Date (year, month, day, hour, minutes);
		elementJson["Date"] = unixTime.getTime();
	}
	
		
	var messagePost = "";
	$(mainContent).find(".userContent").find("p").each(function(index, elementMessage) {
		messagePost = messagePost + $(elementMessage).clone().children().remove().end().text();
	});
	elementJson.Message = messagePost;
		
	var showText = "";
	$(mainContent).find(".userContent").find(".text_exposed_show").find("p").each(function(index, elementMessage) {
		showText = showText + $(elementMessage).clone().children().remove().end().text();
	});
	elementJson.Message = elementJson.Message + showText;
		
	var hashTagsJson = [];
	$(mainContent).find(".userContent").find("._58cn").each(function(index, elementTagPost) {
		hashTagsJson.push($(elementTagPost).text());
	});
	elementJson.Tags = hashTagsJson;
		
		
	var emotionsJson = {};
	$(mainContent).find("._66lg").find("._1n9l").each(function(index, elementEmotion) {
		var typeOfemotion = $(elementEmotion).attr("aria-label").split(": ")[0];
		var countOfemotion = $(elementEmotion).attr("aria-label").split(": ")[1];
			
		if (!!countOfemotion != 0 && (countOfemotion.search(',') != '-1' || countOfemotion.search('тыс') != '-1' || countOfemotion.search('млн') != '-1')) {
			countOfemotion = countOfemotion.replace(",", ".");
			if (countOfemotion.search('млн') != '-1') {
				countOfemotion = (parseFloat(countOfemotion) * 1000000);
			} else {
				countOfemotion = (parseFloat(countOfemotion) * 1000);
			}
		}
			
		if (typeOfemotion == "Нравится") {
			emotionsJson.Like = countOfemotion;
		} else if (typeOfemotion == "Супер") {
			emotionsJson.Love = countOfemotion;
		} else if (typeOfemotion == "Ха-ха") {
			emotionsJson.Haha = countOfemotion;
		} else if (typeOfemotion == "Ух ты!") {	
			emotionsJson.Wow = countOfemotion;
		} else if (typeOfemotion == "Сочувствую") {
			emotionsJson.Sad = countOfemotion;
		} else if (typeOfemotion == "Возмутительно") {
			emotionsJson.Angry = countOfemotion;
		}
	});
	
	elementJson.Emotions = emotionsJson;
		
	elementJson.TotalEmotions = "";
	var totalEmotionPost = $(mainContent).find("._3dlg").text();
		
	if (!!totalEmotionPost != 0 && (totalEmotionPost.search(',') != '-1' || totalEmotionPost.search('тыс') != '-1' || totalEmotionPost.search('млн') != '-1')) {
		totalEmotionPost = totalEmotionPost.replace(",", ".");
		if (totalEmotionPost.search('млн') != '-1') {
			totalEmotionPost = (parseFloat(totalEmotionPost) * 1000000);
		} else {
			totalEmotionPost = (parseFloat(totalEmotionPost) * 1000);
		}
	}
		
	if (!!totalEmotionPost == false) elementJson.TotalEmotions = "";
	else elementJson.TotalEmotions = totalEmotionPost;
		
		
	elementJson.Comment = "";
	var commentPost = $(mainContent).find("._3hg-").text();
	commentPost = commentPost.split(": ")[1];
		
	if (!!commentPost != 0 && (commentPost.search(',') != '-1' || commentPost.search('тыс') != '-1' || commentPost.search('млн') != '-1')) {
		commentPost = commentPost.replace(",", ".");
		if (commentPost.search('млн') != '-1') {
			commentPost = (parseFloat(commentPost) * 1000000);
		} else {
			commentPost = (parseFloat(commentPost) * 1000);
		}
	}
		
	if (!!commentPost == false) elementJson.Comment = "";
	else elementJson.Comment = commentPost;
		
	elementJson.Share = "";
	var sharedPost = $(mainContent).find("._3rwx").text();
	sharedPost = sharedPost.split(": ")[1];
		
	if (!!sharedPost != 0 && (sharedPost.search(',') != '-1' || sharedPost.search('тыс') != '-1' || sharedPost.search('млн') != '-1')) {
		sharedPost = sharedPost.replace(",", ".");
		if (sharedPost.search('млн') != '-1') {
			sharedPost = (parseFloat(sharedPost) * 1000000);
		} else {
			sharedPost = (parseFloat(sharedPost) * 1000);
		}
	}
		
	if (!!sharedPost == false) elementJson.Share = "";
	else elementJson.Share = sharedPost;
	
	var comments = [];
	var commentJson = {};
	$(mainContent).find("._7791").children().each(function (index, content) {
		commentJson = {};
		
		var topOrNot = $(content).find("._4eek.clearfix._4eez.clearfix").find("._3x69").find("._3x6z").text();
		if (topOrNot.length != 0) {
			commentJson.TopUser = true;
		} else {
			commentJson.TopUser = false;
		}
		
		var usernameOfComment = $(content).find("._4eek.clearfix._4eez.clearfix").find("._72vr").find("._6qw4").text();
		commentJson.Username = usernameOfComment;
		
		//var textOfComment = $(content).find("._4eek.clearfix._4eez.clearfix").find("._3l3x").find("span").first().clone().children().remove().end().text();
		var textOfComment = $(content).find("._4eek.clearfix._4eez.clearfix").find("._3l3x").text();
		commentJson.Text = textOfComment;
		
		var countOfEmotionForComent = $(content).find("._4eek.clearfix._4eez.clearfix").find("._1lld").text();
		if (!!countOfEmotionForComent != 0 && (countOfEmotionForComent.search(',') != '-1' || countOfEmotionForComent.search('тыс') != '-1' || countOfEmotionForComent.search('млн') != '-1')) {
			countOfEmotionForComent = countOfEmotionForComent.replace(",", ".");
			if (countOfEmotionForComent.search('млн') != '-1') {
				countOfEmotionForComent = (parseFloat(countOfEmotionForComent) * 1000000);
			} else {
				countOfEmotionForComent = (parseFloat(countOfEmotionForComent) * 1000);
			}
		}
		commentJson.Emotions = countOfEmotionForComent;
		
		var commentedComment = [];
		var commentedJson = {};
		$(content).find("._2h2j").children('ul').children().each(function (ind, cont) {
			commentedJson = {};
			
			var topOrNot = $(content).find("._3x69").find("._3x6z").text();
			if (topOrNot.length != 0) {
				commentJson.TopUser = true;
			} else {
				commentJson.TopUser = false;
			}
			
			commentedJson.Text = $(cont).find("._3l3x").text();
			commentedJson.UserName = $(cont).find("._6qw4").text();
			
			var TotalEmotionsComent = $(cont).find("._1lld").text();
			if (!!TotalEmotionsComent != 0 && (TotalEmotionsComent.search(',') != '-1' || TotalEmotionsComent.search('тыс') != '-1' || TotalEmotionsComent.search('млн') != '-1')) {
				TotalEmotionsComent = TotalEmotionsComent.replace(",", ".");
					if (TotalEmotionsComent.search('млн') != '-1') {
						TotalEmotionsComent = (parseFloat(TotalEmotionsComent) * 1000000);
					} else {
						TotalEmotionsComent = (parseFloat(TotalEmotionsComent) * 1000);
					}
			}
			commentedJson.TotalEmotions = TotalEmotionsComent;
			
			commentedComment.push(commentedJson);
		});
		commentJson.UserCommets = commentedComment;
		
		comments.push(commentJson);
	});
	
	elementJson.Comments = comments;
		
	mainJson.push(elementJson);
	
	var response = JSON.stringify(mainJson, null, '\t');
	response = response.slice(0, -1);
	response = response.slice(1);
	response = response.concat(",");
	
	return Promise.resolve(response);
}


function isVisible(selector) {
  return (document.querySelector(selector) && document.querySelector(selector).clientHeight != 0);
}

var writeFileCostom = async (nameFile, content, message) => {
	
	await fs.writeFile(nameFile, content, 'utf8', (err) => {
		if(err) return console.log(err);
			console.log(message);
	});
	
};

var appendFileCostom = async (nameFile, content, message) => {
	
	await fs.appendFile(nameFile, content, 'utf8', (err) => {
		if(err) return console.log(err);
			console.log(message);
	});
	
};

var getDebugHTMLasFile = async (page, filePath, message) => {
	var content = await page.content();
	await writeFileCostom(filePath, content, message);
};

function removePost() {
	$("._427x").first().remove();
	console.log("removed");
}


var isExist = async (page, selector) => {
	if (!!(await page.$(selector))) {
		return true;
	} else {
		return false;
	}
	
};

function isExistFirstElementComment () {
	if ($("._427x").first().find("._4sxc._42ft[data-testid='UFI2CommentsPagerRenderer/pager_depth_0']").length != 0) {
		return true;
	} else {
		return false;
	}
}

function countOfUserComments () {
	return $("._427x").first().find("._7791").find("._2h2j").length;
}

function waitAjaxRequestUserComment (oldCommentCount) {
	
	if ($("._427x").first().find("._7791").find("._2h2j").length > oldCommentCount) {
		return true;
	} else {
		return false;
	}
	
}

var removeReaded = async (page, count) => {
	
	for (var i = 0; i < count; i++) {
		await page.evaluate(removePost);
	}
}

function countOfElements() {
	return $("._427x").length;
}

function countOfPost() {
  return document.querySelectorAll("._427x").length;
}

function waitAjaxRequest(preCountPost) {
	var postCountPost= document.querySelectorAll("._427x").length;
	if (preCountPost < postCountPost) {
		return true;
	} else {
		return false;
	}
}

var clikСertainCount = async (page, count) => {
	var k = 0;
	
	while (k < count) {
		k++;
		var a = await page.evaluate(isExistFirstElementComment);
		console.log(a);
		await page.waitFor(1 * 1000);
		if (await page.evaluate(isExistFirstElementComment) == false) {
			console.log('Selector does not exist ' + k);
			break;
		} else {
			//await page.click("._4sxc._42ft[data-testid='UFI2CommentsPagerRenderer/pager_depth_0']").then(() => {console.log("The button comment clicked : " + k);});
			await page.keyboard.press('Escape')
				.then(async () => {
					console.log("success Escape pressed");
				})
				.catch(async () => {
					console.log("problem Escape pressed");
				});
				
			var oldCommentCount = await page.evaluate(countOfUserComments);
			
			await page.click("a[data-testid='UFI2CommentsPagerRenderer/pager_depth_0']").then(() => {console.log("The button comment clicked : " + k);});

			await page.waitForFunction(waitAjaxRequestUserComment, {timeout: 30*1000}, oldCommentCount)
				.then(async () => {
					content = await page.content();
					await writeFileCostom("Navigation/successwaitForCommentUser.html", content, "success");
				})
				.catch(async () => {
					content = await page.content();
					await writeFileCostom("Navigation/problemwaitForCommentUser.html", content, "problem");
				});
		}
	}
}

function isExistCommentCommented () {
	if ($("._427x").first().find("._4swz._1i3s._293g").length != 0) {
		return true;
	} else {
		return false;
	}
}

var clickCommentCommented = async (page) => {
	while (true) {
		var a = await page.evaluate(isExistCommentCommented);
		//var b = await isExist(page, "._5pcr.userContentWrapper a[data-testid='UFI2CommentsPagerRenderer/pager_depth_1']");
		console.log("hello 1" + a);
		
		if (a) {
			await page.keyboard.press('Escape')
				.then(async () => {
					console.log("success Escape pressed");
				})
				.catch(async () => {
					console.log("problem Escape pressed");
				});
				
			await page.click("._5pcr.userContentWrapper a[data-testid='UFI2CommentsPagerRenderer/pager_depth_1']")
				.then(() => {console.log("The button commented comment clicked : ");})
				.catch(() => {console.log("The problem");});
			
			await page.keyboard.press('Escape')
				.then(async () => {
					console.log("success Escape pressed");
				})
				.catch(async () => {
					console.log("problem Escape pressed");
				});
				
		} else {
			console.log("clicked all commented comment");
			break;
		}
	}
	await getDebugHTMLasFile(page, "qwertytrtru.html", "debug done");
}

var Links = [];
var ref = "https://www.facebook.com/search/str/%23"; 
var request = "putin";
var uniqRef = "https://www.facebook.com/search/str/%23putin/stories-keyword/stories-public";

var uniqRef1 = "https://www.facebook.com/pg/flumemusic/posts/";

(async () => {
	const browser = await puppeteer.launch({args: ['--disable-dev-shm-usage']});
	const page = await browser.newPage();
	
	var metric = await page.metrics();
	var pre = metric["Timestamp"];
	var post = pre;
	var sum = 0;
	
	await page.goto('https://facebook.com');
	await page.setViewport({width: 1920, height: 1080});
	
	await page.type('#email', '89650383509'); 
	await page.type('#pass', 'daurenqwerty', {delay: 100}); 
	await page.click('[aria-label="Войти"]');
	await page.setViewport({width: 1920, height: 1080}); 
	
	await page.goto(uniqRef1);
	await page.addScriptTag({path: require.resolve('jquery')});
	await page.waitFor(5 * 1000);
	
	var countParsedPost = 0;
	
	//await removeReaded(page);
	/*await getDebugHTMLasFile(page, "pre.html", "Done ");
	
	await page.click(".pam.uiBoxLightblue.uiMorePagerPrimary").then(() => {console.log('Clicked');});
	await page.waitFor(5 * 1000);
	await getDebugHTMLasFile(page, "post.html", "Done ");*/
	
	
	while (true) {
		
		if (countParsedPost % 50 == 0 && countParsedPost != 0) {
			console.log('The next big step');
			await page.setViewport({width: 1920, height: 1080}); 
			
			await page.goto("about:blank");
			await page.waitFor(5 * 1000);
			
			await page.goto(uniqRef1, {timeout: 120*1000});
			
			await page.addScriptTag({path: require.resolve('jquery')});
			await page.waitFor(5 * 1000);
			
			while (true) {
				if (await page.evaluate(countOfElements) > countParsedPost || (await isExist(page, ".pam.uiBoxLightblue.uiMorePagerPrimary")) == false) {	
					await removeReaded(page, countParsedPost);
					await getDebugHTMLasFile(page, "removed.html", "removed");
					break;
				} else {
					//await page.waitFor(10 * 1000);
					var len = await page.evaluate(countOfPost);
					await page.waitFor(500);
					await page.click(".pam.uiBoxLightblue.uiMorePagerPrimary").then(() => {console.log('Clicked lowest button');});
					console.log("len " + len);
					
					await page.waitForFunction(waitAjaxRequest, {timeout: 120*1000}, len)
					.then(async () => {
						content = await page.content();
						await writeFileCostom("Navigation/successwaitForNavigation.html", content, "The Third waiter has success");
					})
					.catch(async () => {
						content = await page.content();
						await writeFileCostom("Navigation/problemwaitForNavigation.html", content, "The Third waiter has problem");
					});
					
					//await page.waitFor(5 * 1000);
				}
			}
		}
		if (await isExist(page, "div[class='_427x']")) {
			console.log("qwertyuiuytr");
			await clikСertainCount(page, 2);
			await page.keyboard.press('Escape')
				.then(async () => {
					console.log("success Escape pressed");
				})
				.catch(async () => {
					console.log("problem Escape pressed");
				});
			await clickCommentCommented(page);
			await page.waitFor(1 * 1000);
			
			await getDebugHTMLasFile(page, "qwertyu.html", "debug done");
			var result = await page.evaluate(fetchData);
			await appendFileCostom("posts.json", result, "The " + countParsedPost + " data was saved!");
			
			metric = await page.metrics();
			post = metric["Timestamp"];
			var s = (post - pre)/60; 
			pre = post;
			
			sum = sum + s;
			
			await appendFileCostom("Metrics/successMetrics.text",  sum + '\n', "Success Mtrics file was saved");
			
			await page.evaluate(removePost);
			console.log("This is last post which removed  " + countParsedPost);
			
			//await page.waitFor();
			await getDebugHTMLasFile(page, "test/page" + countParsedPost + ".html", "debug done");

			var cc = await page.evaluate(countOfPost);
			
			if (cc < 4) {
				if ((await isExist(page, ".pam.uiBoxLightblue.uiMorePagerPrimary")) != false) {
					var cc = await page.evaluate(countOfPost);
					await scrollPageToBottom(page);
					
					console.log("                         " + cc);
					await page.waitForFunction(waitAjaxRequest, {timeout: 120*1000}, cc)
						.then(async () => {
							content = await page.content();
							await writeFileCostom("Navigation/successwaitForSrolled.html", content, "Srolled success");
						})
						.catch(async () => {
							content = await page.content();
							await writeFileCostom("Navigation/problemwaitForSrolled.html", content, "Not Scrolled");
						});
					
					var c1 = await page.evaluate(countOfPost);
					console.log("                         " + c1);
				}
			}
		
			countParsedPost++;
		} else {
			console.log('The end');
			break;
		}
	}
	
	await browser.close();
})();


