package tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseText {
    private static final String regexPostForFistPage = ">\\s*<div>\\s*<p>(?<postText>.*?)\\s*</p>";
    private static final String regexAsyncGetToken = "\"async_get_token\":\"(?<asyncGetToken>.*?)\"}";
    private static final String regexGlobalDate = "},\\s*globalData:(?<globalDate>.*?)}},";
    private static final String regexCursorNumberEmTr = ",\\s*\\[\\{cursor:(?<CursorNumberEmTr>.*?),display";
    private static final String regexCursorNumberEmTrForNextPage = ",\\s*\\[\\{\"cursor\":(?<CursorNumberEmTrNext>.*?),\"display";
    private static final String theLastElementOfNextRequest = "&__user=100024759699614&__a=1&__dyn=7AgNe-4amaxx2u6aJGeFxqewKKEKEW8x2C-C267Uqzob4q2i5U4e2DwZx-EK6UnG2OUuKewXGt0TyKdwJx64e2u5Ku58O5UlwQwEyoiwBx61zwzwnqxWeCxy1wDBwm88ofHG7ooxu6Uao4a11x-2KdUcUaEsx7G486mcyV8y4Ehyo8Ja6UylxfwzBzUOGyprxCfxbCwjEfqGu2K6ohgG4eeKi8wGwFyFEKU4u2m48y2i3ufCU&__req=16&__be=1&__pc=PHASED:DEFAULT&__rev=4393189&__spin_r=4393189&__spin_b=trunk&__spin_t=1538887793";
    private static final String startNextURL = "https://www.facebook.com/ajax/pagelet/generic.php/BrowseScrollingSetPagelet?dpr=1&fb_dtsg_ag=";

    private Matcher matcher;

    public void parsePostsFromFirstPage(StringBuilder stringBuilder) {
        matcher = Pattern.compile(regexPostForFistPage).matcher(stringBuilder);

        while (matcher.find()) {
            System.out.print(matcher.group("postText"));
            System.out.print("\n");
        }
    }

    public String getURLFormNextPage(StringBuilder stringBuilder) {
        StringBuilder nextFullRequest = new StringBuilder();

        matcher = Pattern.compile(regexAsyncGetToken).matcher(stringBuilder);
        while (matcher.find()) {
            nextFullRequest.append(startNextURL).append(matcher.group("asyncGetToken"));
            //structureOfURL.setAsyncGetToken(matcher.group("asyncGetToken"));
        }

        matcher = Pattern.compile(regexGlobalDate).matcher(stringBuilder);
        while (matcher.find()) {
            String globalDate = correcterGlobalDate(matcher.group("globalDate"));

            nextFullRequest.append("&data=").append(globalDate).append("},");
            //structureOfURL.setGlobalDate("&data=" + matcher.group("globalDate") + "},");
        }

        matcher = Pattern.compile(regexCursorNumberEmTr).matcher(stringBuilder);
        while (matcher.find()) {
            String cursorNumberEmTr = correcterCursorNumberEmTr(matcher.group("CursorNumberEmTr"));

            nextFullRequest.append("\"cursor\":").append(cursorNumberEmTr).append("}").append(theLastElementOfNextRequest);
            //structureOfURL.setCursorNumberEmTr("\"cursor\":" + matcher.group("CursorNumberEmTr") + "}");
        }

        return nextFullRequest.toString();
    }

    /*public void parseNextPage (StringBuilder contentOfPage, StructureOfURL structureOfURL) { // Пока не роботает как
        StringBuilder nextFullRequest = new StringBuilder();
        matcher = Pattern.compile(regexCursorNumberEmTrForNextPage).matcher(contentOfPage);
        while (matcher.find()) {
            nextFullRequest.append(matcher.group("CursorNumberEmTrNext")).append("}");
        }
        System.out.println(nextFullRequest.toString());
    }*/

    private String correcterGlobalDate(String s) {
        StringBuilder rightGlobalDate = new StringBuilder("{\"v");

        for (int i = 2; i < s.length(); i++) {
            if (s.charAt(i-1) == ',' && (('a' <= s.charAt(i) && s.charAt(i) <= 'z') || ('A' <= s.charAt(i) && s.charAt(i) <= 'Z'))) {
                rightGlobalDate.append('"');
            } else if (s.charAt(i) == ':' && (('a' <= s.charAt(i-1) && s.charAt(i-1) <= 'z') || ('A' <= s.charAt(i-1) && s.charAt(i-1) <= 'Z'))) {
                rightGlobalDate.append('"');
            } else if (s.charAt(i) == 'c' && s.charAt(i+1) == 'r' && s.charAt(i+2) == 'c' && s.charAt(i+3) == 't') {
                rightGlobalDate.append('"');
            }
            rightGlobalDate.append(s.charAt(i));
        }


        return rightGlobalDate.toString();
    }

    private String correcterCursorNumberEmTr(String s) {
        StringBuilder rightGlobalDate = new StringBuilder("\"");

        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i-1) == ',' && (('a' <= s.charAt(i) && s.charAt(i) <= 'z') || ('A' <= s.charAt(i) && s.charAt(i) <= 'Z'))) {
                rightGlobalDate.append('"');
            } else if (s.charAt(i) == ':' && (('a' <= s.charAt(i-1) && s.charAt(i-1) <= 'z') || ('A' <= s.charAt(i-1) && s.charAt(i-1) <= 'Z'))) {
                rightGlobalDate.append('"');
            }
            rightGlobalDate.append(s.charAt(i));
        }


        return rightGlobalDate.toString();
    }

}