package tools;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SendGetRequest {
    private static final String onePartOfQuery = "https://www.facebook.com/search/str/%23";
    private static final String secondPartOfQuery = "/stories-keyword/stories-public";
    private String hashTag;
    private String urlQuery;

    public void setUrlQueryToFirst () {
        urlQuery = onePartOfQuery + hashTag + secondPartOfQuery;
    }

    public void setUrlQuery (String s) {
        urlQuery = s;
    }

    public String getHashTag() {
        return hashTag;
    }

    public void setHashTag(String hashTag) {
        this.hashTag = hashTag;
    }

    public StringBuilder getHtml () {
        String URL  = urlQuery;
        StringBuilder stringBufferHtml = new StringBuilder();
        HttpURLConnection connection = null;

        try {
            connection = (HttpURLConnection) new URL(URL).openConnection();
            connection.setRequestMethod("GET");
            connection.setUseCaches(false);

            connection.setRequestProperty("Cookie",
                    "sb=JZGnWxEMsovADwGoWc8ixCf_; datr=JZGnW7n2XWGsftv-wCDDahcV; locale=ru_RU; c_user=100024759699614; xs=18%3AteS2WMwkywiJyA%3A2%3A1539452613%3A7567%3A8458; fr=0i3aFVk65aWEMJ0cG.AWVAz_DsKhM4JZXln7V-iBRku7I.BblYiI.SW.AAA.0.0.Bbwi7F.AWU76Bb0; pl=n; spin=r.4418549_b.trunk_t.1539452614_s.1_v.2_; wd=1920x526; presence=EDvF3EtimeF1539455514EuserFA21B24759699614A2EstateFDt3F_5b_5dG539455514560CEchFDp_5f1B24759699614F4CC"
            );

            connection.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");

            connection.setConnectTimeout(11000000);
            connection.setReadTimeout(11000000);

            connection.connect();

            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                FileWriter fileWriter = new FileWriter("html.txt", false);

                String line;
                while ((line = in.readLine()) !=  null) {
                    fileWriter.write(line);
                    fileWriter.write("\n");
                    stringBufferHtml.append(line);
                }

                fileWriter.close();
            } else {
                System.out.println("fail: " + connection.getResponseCode() + ", " + connection.getResponseMessage());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return stringBufferHtml;
    }

}
