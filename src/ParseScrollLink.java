import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLDecoder;

public class ParseScrollLink {

    public static void parseLinkComfortOrder (String link) {

        try {
            link = URLDecoder.decode(link, "UTF-8");
            FileWriter fileWriter = new FileWriter("link.txt", true);
            fileWriter.append('\n'); fileWriter.append('\n'); fileWriter.append('\n');

            for (int i = 0; i < link.length(); i++) {
                if (link.charAt(i) == ',') {
                    fileWriter.append(link.charAt(i));
                    fileWriter.append('\n');
                } else
                    fileWriter.append(link.charAt(i));
            }

            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
