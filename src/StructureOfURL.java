public class StructureOfURL {
    private static final String startNextURL = "https://www.facebook.com/ajax/pagelet/generic.php/BrowseScrollingSetPagelet?dpr=1&fb_dtsg_ag=";
    private static final String theLastElementOfNextRequest = "&__user=100024759699614&__a=1&__dyn=7AgNe-4amaxx2u6aJGeFxqewKKEKEW8x2C-C267Uqzob4q2i5U4e2DwZx-EK6UnG2OUuKewXGt0TyKdwJx64e2u5Ku58O5UlwQwEyoiwBx61zwzwnqxWeCxy1wDBwm88ofHG7ooxu6Uao4a11x-2KdUcUaEsx7G486mcyV8y4Ehyo8Ja6UylxfwzBzUOGyprxCfxbCwjEfqGu2K6ohgG4eeKi8wGwFyFEKU4u2m48y2i3ufCU&__req=16&__be=1&__pc=PHASED:DEFAULT&__rev=4393189&__spin_r=4393189&__spin_b=trunk&__spin_t=1538887793";

    private String asyncGetToken;
    private String globalDate;
    private String CursorNumberEmTr;

    public StructureOfURL() {}

    public String getAsyncGetToken() {
        return asyncGetToken;
    }

    public void setAsyncGetToken(String asyncGetToken) {
        this.asyncGetToken = asyncGetToken;
    }

    public String getGlobalDate() {
        return globalDate;
    }

    public void setGlobalDate(String globalDate) {
        this.globalDate = globalDate;
    }

    public String getCursorNumberEmTr() {
        return CursorNumberEmTr;
    }

    public void setCursorNumberEmTr(String cursorNumberEmTr) {
        CursorNumberEmTr = cursorNumberEmTr;
    }

}
